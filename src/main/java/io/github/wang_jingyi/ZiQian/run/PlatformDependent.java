package io.github.wang_jingyi.ZiQian.run;

public class PlatformDependent {
	
	public static final String USER_HOME_DIR = System.getProperty("user.home");
	public static final String CAV_MODEL_ROOT = USER_HOME_DIR +"/ziqian_evaluation_cav";
	public static final String PRISM_PATH = USER_HOME_DIR +"/prism-4.3-osx64/bin/prism";
	public static final String SWAT_SIMULATE_PATH = USER_HOME_DIR +"/swat_beta_v2_1";
	public static final String SWAT_EVALUATION_PATH = USER_HOME_DIR + "/swat_dataset";
	
}

