package io.github.wang_jingyi.ZiQian;

import java.io.IOException;


public interface FormatVariablesValue {
	
	public VariablesValueInfo getVariablesValueInfo() throws IOException;
	
}
