package io.github.wang_jingyi.ZiQian.active;

import org.apache.commons.math3.linear.RealMatrix;

public interface InitialTrainPhase {
	
	public RealMatrix getInitialFrequencyMatrix();

}
