package io.github.wang_jingyi.ZiQian;

import java.util.List;


public interface Abstraction {
	
	public List<String> abstractList(List<VariablesValue> vv);
	
}
